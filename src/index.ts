import express from 'express'
const app = express()
const { SERVER_PORT = 8080 } = process.env

app.get('/', (req, res) => res.send('Express + TypeScript Server'))
app.listen(SERVER_PORT, () => {
  console.log(
    `⚡️[server]: Server is running at http://localhost:${SERVER_PORT}`
  )
})
